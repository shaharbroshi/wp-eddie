<?php
global $post;

$codes = get_field('global_codes', 'options');
$thankyoupage = get_field('thankyou_link', 'options');

// Footer WhatsApp
$whatsapp_status = get_field('whatsapp_status', 'options');
$whatsapp_number = get_field('whatsapp_number', 'options');
$whatsapp_intro = get_field('whatsapp_intro', 'options');
if($whatsapp_status){ $fixed_strip_class = " cols3"; }else { $fixed_strip_class = ""; }

$footer_links_text = get_field('footer_links_text', 'options');

$strip_bottom = get_field('text-bottom', 'options');

$footer_settings = get_field('footer_settings', 'options');
$footer_col_1 = $footer_settings['footer_col_1'];
$footer_col_2 = $footer_settings['footer_col_2'];
$footer_col_3 = $footer_settings['footer_col_3'];
$footer_col_4 = $footer_settings['footer_col_4'];
$image_reco = $footer_settings['image_reco'];

$leave_feedback = get_field('leave_feedback', 'options');
$feedback_content = get_field('feedback_content', 'options');

?>

<?php if($strip_bottom) { ?>
    <div class="fixed_strip hide_mobile">
        <img src="<?= asset('images/phone.png') ?>" class="phone" />
        <?= $strip_bottom; ?>
        <img src="<?= asset('images/c_button.png'); ?>" class="c_button">
    </div>
<?php } ?>

<div class="footer_mobile_strip hide_desktop<?= $fixed_strip_class; ?>">
    <a href="#" class="phone">
        <img src="<?= asset('images/phone-call.svg') ?>" />
        חייג עכשיו
<!--        <span class="small">(זמינות 24 שעות)</span>-->
    </a>
    <a href="#contactform" class="contact">
        <img src="<?= asset('images/envelope-shape.svg') ?>" />
        צור קשר
    </a>

    <?php if($whatsapp_status) { ?>
        <a href="https://wa.me/<?= $whatsapp_number; ?>/?text=<?= $whatsapp_intro; ?>" class="whatsapp">
            <img src="<?= asset('images/whats.svg') ?>" />
            פנה בוואטסאפ
        </a>
    <?php } ?>

</div>

<div class="fixed_form">
    <a href="#" class="close_fixed_form">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </a>
    <span class="span_title_big">פנייה לייעוץ ראשוני <span class="break-mobile">ללא התחייבות</span></span>
    <?php echo do_shortcode('[contact-form-7 title="טופס אחרי מאמר"]'); ?>
</div>

<div class="side_menu_mobile">
    <span class="span_title_big"><?= get_field('title-moreinfo', 'options'); ?></span>
    <?php wp_nav_menu( array( 'menu' => 'side-menu', 'menu_class' => 'side-menu' ) ); ?>
</div>

<?php if(!$footer_settings['footer_hide']) { ?>

<footer>
    <div class="container">

        <div class="footer_cols <?php if($image_reco['url']) { ?>grid-5<?php } ?>">

            <div class="fcol">

                <span class="fcol-title"><?= $footer_col_1['title']; ?></span>
                <ul class="fcol-menu">
                    <?php foreach($footer_col_1['links'] as $col1_links){ ?>
                        <li><a href="<?= $col1_links['link']['url']; ?>" title="<?= $col1_links['link']['title']; ?>" <?php if($col1_links['link']['target']) { ?>target="_blank"<?php } ?>><?= $col1_links['link']['title']; ?></a></li>
                    <?php } ?>
                </ul>

            </div>

            <div class="fcol">

                <span class="fcol-title"><?= $footer_col_2['title']; ?></span>
                <ul class="fcol-menu">
                    <?php foreach($footer_col_2['links'] as $col2_links){ ?>
                        <li><a href="<?= $col2_links['link']['url']; ?>" title="<?= $col2_links['link']['title']; ?>" <?php if($col2_links['link']['target']) { ?>target="_blank"<?php } ?>><?= $col2_links['link']['title']; ?></a></li>
                    <?php } ?>
                </ul>

            </div>

            <div class="fcol">

                <span class="fcol-title"><?= $footer_col_3['title']; ?></span>
                <ul class="fcol-menu">
                    <?php foreach($footer_col_3['links'] as $col3_links){ ?>
                        <li><a href="<?= $col3_links['link']['url']; ?>" title="<?= $col3_links['link']['title']; ?>" <?php if($col3_links['link']['target']) { ?>target="_blank"<?php } ?>><?= $col3_links['link']['title']; ?></a></li>
                    <?php } ?>
                </ul>


            </div>

            <div class="fcol">

                <span class="fcol-title"><?= $footer_col_4['title']; ?></span>

                <div class="fcol-text">
                    <?php
                    $contact_details = get_field('contact-details', 'options');

                    if($contact_details){
                        echo $contact_details;
                    }else{
                        echo $footer_col_4['text'];
                    }
                    ?>
                </div>

                <div class="fcol-social">

                    <?php
                    if(is_array($footer_col_4['social'])){

                        foreach($footer_col_4['social'] as $social){

                            ?><a href="<?= $social['link']; ?>" target="_blank"><img src="<?= $social['image']['url']; ?>" /></a><?php

                        }

                    }
                    ?>

                </div>

            </div>

            <?php if($image_reco['url']) { ?>
                <div class="fcol">
                    <?php if (isset($image_reco['url']) && $image_reco['url']) { ?>
                        <img src="<?= $image_reco['url']; ?>" alt="<?= $image_reco['alt']; ?>">
                    <?php } ?>
                </div>
            <?php } ?>

        </div>

    </div>
</footer>

<?php } ?>

</div>


<?php if($leave_feedback) { ?>
<div id="feedback-popup" class="white-popup mfp-hide">

    <div class="heading">

        <div class="title">
            <?php echo $feedback_content['title']; ?>
        </div>

        <div class="subtitle">
            <?php echo $feedback_content['text']; ?>
        </div>

    </div>

    <div class="content">
        <?php echo do_shortcode('[contact-form-7 id="442" title="טופס פידבק לאתר"]'); ?>

        <div class="thanks">
            קבלו מראש את תודתנו והערכתנו הכנה!
        </div>
    </div>


</div>
<?php } ?>

<?php if($codes['body_code']) { ?>
    <!-- Body Code -->
    <?= $codes['body_code']; ?>
    <!-- Body Code -->
<?php } ?>

<?php
if(is_array($thankyoupage)) {
    $thankyou_id = url_to_postid($thankyoupage['url']);

    if($thankyou_id == $post->ID){
        ?>
        <!-- Thankyou page Body code -->
        <?= $codes['thankyou_body']; ?>
        <!-- Thankyou page Body code -->
        <?php
    }

}
?>

<!-- BitBucket Theme v3 -->
<!-- e.o site-wrap -->
<?php wp_footer(); ?>

</body>
</html>
