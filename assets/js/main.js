jQuery(document).ready(function($) {

    var header = $('.header_mobile');
    var menu_mobile = $('.menu_mobile');
    var fixed_form = $('.fixed_form');

    // sticky menu
    var lastScrollTop = 0;
    $(window).scroll(function(){
        var scrollTop = 2;
        var st = $(this).scrollTop();

        if($(window).scrollTop() >= scrollTop + 200){
            header.addClass('header-scrolled');
            menu_mobile.addClass('menu-sticky');
            if (st > lastScrollTop){
                header.addClass('scroll-up').removeClass('scroll-down');
            } else {
                header.addClass('scroll-down').removeClass('scroll-up');
            }
        }
        else {
            header.addClass('scroll-down').removeClass('scroll-up');
            header.removeClass('header-scrolled');
            menu_mobile.removeClass('menu-sticky');
        }
        lastScrollTop = st;
    });

    // $('.footer_mobile_strip a.contact, a.close_fixed_form').on('click', function(e){
    //    e.preventDefault();
    //
    //    var $fixed_form = $('.fixed_form');
    //
    //     if ($fixed_form.hasClass('open')) {
    //         $('body').removeClass('sticky');
    //         $fixed_form.removeClass('open');
    //     }else{
    //         $('body').addClass('sticky');
    //         $fixed_form.addClass('open');
    //     }
    //
    // });


    // first check if a[href="#contact-popup"] exists
    if ($('a[href="#feedback-popup"]').length) {
        $('a[href="#feedback-popup"]').magnificPopup({
            type:'inline',
            midClick: true,
        });
    }



    $('.header_mobile_btn').on('click', function(e){

        if ($(this).hasClass('open')) {
            $('body').removeClass('sticky');
            $(this).removeClass('open');
            $('.menu_mobile').removeClass('open');
        }else{
            $('body').addClass('sticky');
            $(this).addClass('open');
            $('.menu_mobile').addClass('open');
        }

    });

    $('.menu_mobile li.menu-item-has-children > a').on('click', function(e){
        e.preventDefault();

        var submenu = $(this).siblings('ul.sub-menu');
        var liparent = $(this).parent();

        if( submenu.hasClass('show') ){
            submenu.removeClass('show');
            submenu.fadeOut();
            liparent.removeClass('active');
        }else{
            submenu.addClass('show');
            submenu.fadeIn();
            liparent.addClass('active');
        }

        // $(this).siblings('ul.sub-menu').addClass('show');

    });


    // Contact Form 7 Init
    initWPCF7Loader();
    initWPCF7AddErrorClass();

    // Contact Form 7 Init Functions
    function initWPCF7Loader() {
        // on click submit add loading class
        $('.wpcf7-submit').on('click', function () {
            var $form = $(this).closest('form');
            $form.addClass('form-loading');
        });

        // remove loading class
        document.addEventListener('wpcf7submit', removeLoading, false);
        document.addEventListener('wpcf7invalid', removeLoading, false);
        document.addEventListener('wpcf7mailsent', removeLoading, false);
        document.addEventListener('wpcf7mailfailed', removeLoading, false);

        function removeLoading(event) {
            var $form = $(event.target).find('form');
            $form.removeClass('form-loading');
        }
    }

    function initWPCF7AddErrorClass() {
        // remove loading class
        document.addEventListener('wpcf7submit', updateErrorClass, false);
        document.addEventListener('wpcf7invalid', updateErrorClass, false);
        document.addEventListener('wpcf7mailsent', updateErrorClass, false);
        document.addEventListener('wpcf7mailfailed', updateErrorClass, false);

        function updateErrorClass(event) {
            var $form = $(event.target).find('form');
            $form.find('.input').removeClass('error');
            $form.find('.wpcf7-not-valid').each(function () {
                $(this).closest('.input').addClass('error');
            });
        }
    }

    // // Scroll to Anchor in Animate
    // $(function() {
    //     $('a[href*="#"]:not([href="#"])').click(function() {
    //         if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

    //             var target = $(this.hash);
    //             target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
    //             if (target.length) {
    //                 $('html,body').animate({
    //                     scrollTop: target.offset().top - $('.header_mobile').innerHeight() - 10
    //                 }, 1000);
    //                 return false;
    //             }
    //         }
    //     });
    // });

    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - $('.header_mobile').innerHeight() - 10
                }, 1000);
                return false;
            }
        }
    });

    // Ref Parameter
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    };

    var siteRef = getUrlParameter('ref');
    var refCookie = $('.cookie-ref').attr('data-ref');
    var refCookiePhone = $('.cookie-phone').attr('data-phone');

    if(siteRef) {
        setWebcookie(refCookie);
        setWebcookiePhone(refCookiePhone);
    }

    $('span.ref_phone').text($.cookie("refcookiephone"));

    if($.cookie("refcookiephone")){
        $('a.phone').attr('href', 'tel:' + $.cookie("refcookiephone"));
    }else{
        $('a.phone').attr('href', 'tel:' + $('.fixed_strip span.ref_phone').text());
    }

    // Desktop sticky footer close
    $('.c_button').on('click', function(e){
        e.preventDefault();

        $('.fixed_strip').addClass('hide');

    });

    $('a.faq-question').on('click', function(e){
       e.preventDefault();

       var question_id = $(this).attr('data-question');
       var answer_div = $('.faq-answer[data-question=' + question_id + ']');

       if(answer_div.hasClass('show')){
           answer_div.removeClass('show');
           answer_div.slideUp();
       }else{
           answer_div.addClass('show');
           answer_div.slideDown();
       }

    });

    function initFixIframeVideoAspectRatio() {

        // Find all YouTube videos
        // Expand that selector for Vimeo and whatever else
        var $allVideos = $(".single_post_content iframe");

        // Figure out and save aspect ratio for each video
        $allVideos.each(function() {

            $(this)
                .data('aspectRatio', this.height / this.width)

                // and remove the hard coded width/height
                .removeAttr('height')
                .removeAttr('width');

        });

        // When the window is resized
        $(window).resize(function() {

            // Resize all videos according to their own aspect ratio
            $allVideos.each(function() {

                var newWidth = $(this).parent().width();

                var $el = $(this);
                $el
                    .width(newWidth)
                    .height(newWidth * $el.data('aspectRatio'));

            });

            // Kick off one resize to fix all videos on page load
        }).resize();

    }

    initFixIframeVideoAspectRatio();


});

function setWebcookie($kind) {
    $.cookie("refcookie", $kind, {expires: 7, path: '/'});
}

function setWebcookiePhone($kind) {
    $.cookie("refcookiephone", $kind, {expires: 7, path: '/'});
}
