jQuery(document).ready(function($) {

    function initTOC({tocSelector, contentSelector, headingSelector}) {

        // Check if headingsSelector is array and convert to string
        if (Array.isArray(headingSelector)) {
            headingSelector = headingSelector.join(',');
        }

        // Adding anchor to headingsSelector
        let headingsFound = makeHeadingsIDs(contentSelector, headingSelector);

        tocbot.init({
            // Where to render the table of contents.
            tocSelector,
            // Where to grab the headings to build the table of contents.
            contentSelector,
            // Which headings to grab inside of the contentSelector element.
            headingSelector,
            // Other options
            scrollSmooth: true,
            scrollSmoothOffset: -20,
            headingsOffset: 140,
            scrollSmoothDuration: 1000,
            orderedList: false,
            listClass: 'post__toc-tocList',
            listItemClass: 'post__toc-tocList-item',
            // Headings that match the ignoreSelector will be skipped.
            // ignoreSelector: '.js-toc-ignore',
        });

        // Helper
        function makeHeadingsIDs(contentSelector, headingsSelectorArray, ignoreSelector = '') {
            if (!headingsSelectorArray.length) return false;

            let content = $(contentSelector);
            let headings = $(content).find(headingsSelectorArray).not(ignoreSelector);

            let headingMap = {}

            $(headings).each(function () {

                let heading = this;

                let id = $(heading).attr('id')
                    ? $(heading).attr('id')
                    : $(heading).text().trim().toLowerCase()
                        .split(' ').join('-').replace(/[!@#$%^&*?.():]/ig, '').replace(/\//ig, '-')
                headingMap[id] = !isNaN(headingMap[id]) ? ++headingMap[id] : 0
                if (headingMap[id]) {
                    $(heading).attr('id', id + '-' + headingMap[id])
                } else {
                    $(heading).attr('id', id)
                }
            })

            return true;
        }


    }

    var toc_element = $('.post__toc');

    // Table Of Contents
    if(toc_element.length > 0){
        $('body').addClass('toc-active');

        initTOC({
            tocSelector: '.post__toc-explore',
            // contentSelector: '.single_post_content',
            contentSelector: '.page_content',
            headingSelector: $('.post__toc-explore').attr('data-toc-titles')
        });

    }

    // Hide Items greater than 5
    var i = 0;

    $('.post__toc-tocList').find('li').each(function(){

        i++;

        var svgIcon = `
        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M10.729 2.712C10.9589 2.48216 11.2358 2.30486 11.5408 2.1923C11.8457 2.07975 12.1715 2.0346 12.4955 2.05997C12.8196 2.08535 13.1343 2.18064 13.418 2.33929C13.7018 2.49794 13.9477 2.71619 14.139 2.979L14.459 3.419L7.98603 9.893L7.60603 9.64C7.32913 9.45527 7.09666 9.2114 6.92539 8.92597C6.75413 8.64055 6.64832 8.32068 6.61561 7.98942C6.58289 7.65817 6.62408 7.32378 6.73621 7.01037C6.84834 6.69696 7.02861 6.41233 7.26403 6.177L10.729 2.712V2.712ZM9.25903 10.742L11.821 12.449C11.9077 12.5072 11.9813 12.5828 12.037 12.671L13.271 14.609L19.282 8.597L17.132 7.034C17.0687 6.98764 17.013 6.93163 16.967 6.868L15.353 4.648L9.25903 10.742V10.742ZM14.363 16.326L14.096 15.906L20.51 9.49L21.02 9.862C21.2829 10.0532 21.5012 10.2991 21.66 10.5828C21.8187 10.8665 21.9141 11.1812 21.9396 11.5053C21.965 11.8293 21.92 12.1551 21.8075 12.4601C21.695 12.7651 21.5178 13.0421 21.288 13.272L17.852 16.709C17.6138 16.9474 17.325 17.1292 17.0071 17.241C16.6891 17.3529 16.3501 17.3918 16.0151 17.3551C15.6801 17.3183 15.3576 17.2067 15.0715 17.0286C14.7854 16.8505 14.5429 16.6104 14.362 16.326H14.363ZM9.34103 12.299L3.26803 18.257C3.09625 18.4178 2.95846 18.6114 2.86281 18.8264C2.76715 19.0414 2.71558 19.2733 2.71113 19.5086C2.70668 19.7439 2.74945 19.9776 2.83691 20.1961C2.92437 20.4145 3.05474 20.6132 3.22032 20.7804C3.3859 20.9475 3.58331 21.0798 3.8009 21.1694C4.01849 21.259 4.25182 21.304 4.48711 21.3018C4.7224 21.2996 4.95486 21.2503 5.17075 21.1567C5.38664 21.0631 5.58157 20.9272 5.74403 20.757L11.829 14.672L11.039 13.431L9.34103 12.299V12.299ZM14.75 19C14.5511 19 14.3603 19.079 14.2197 19.2197C14.079 19.3603 14 19.5511 14 19.75C14 19.9489 14.079 20.1397 14.2197 20.2803C14.3603 20.421 14.5511 20.5 14.75 20.5H12.75C12.5511 20.5 12.3603 20.579 12.2197 20.7197C12.079 20.8603 12 21.0511 12 21.25C12 21.4489 12.079 21.6397 12.2197 21.7803C12.3603 21.921 12.5511 22 12.75 22H21.25C21.4489 22 21.6397 21.921 21.7804 21.7803C21.921 21.6397 22 21.4489 22 21.25C22 21.0511 21.921 20.8603 21.7804 20.7197C21.6397 20.579 21.4489 20.5 21.25 20.5H19.25C19.4489 20.5 19.6397 20.421 19.7804 20.2803C19.921 20.1397 20 19.9489 20 19.75C20 19.5511 19.921 19.3603 19.7804 19.2197C19.6397 19.079 19.4489 19 19.25 19H14.75Z" fill="#000"/>
        </svg>
        `;

        $(this).prepend(svgIcon);

        if(i > 5){
            $(this).addClass('hide-me');
            $(this).hide();
            $('.post__toc-expand').show();
        }


    });

    $('.post__toc-expand-items').on('click', function(e){
        e.preventDefault();

        post_toc_expand();
    });

    function post_toc_expand(){

        $('.post__toc-tocList .post__toc-tocList-item').removeClass('hide-me').slideDown();
        $('.post__toc-expand').hide();

    }



});
