<?php
get_header();

global $post;

// Article Settings
$article_settings = get_field('article_settings', $post->ID);

// Faq
$faq = get_field('faq', $post->ID);
$faq_r = $faq['faq_r'];

$infobox_hide = $article_settings['infobox_hide'];
if($infobox_hide) { $wrapper_class = " onecol"; }else{ $wrapper_class = ""; }

// Big Image
$big_image = get_field('big-image', 'options');

$children = get_pages( array( 'child_of' => $post->ID ) );

// Sidebar location - right or left?
$mainwrap_location = get_field('mainwrap_location', 'options');

$mainwrap_class = '';
if($mainwrap_location == "left"){
    $mainwrap_class = " reverse";
}
?>

<div class="content_wrap container<?= $mainwrap_class; ?>">

    <?php print_block_right(); ?>

    <div class="block_left">

        <?php
        if ( have_posts() ) {
            while ( have_posts() ) {
                the_post();

                ?>
                <div class="page_content">

                    <?php if ( function_exists('yoast_breadcrumb') ) { ?>
                        <div class="breadcrumbs">
                            <?php yoast_breadcrumb(); ?>
                        </div>
                    <?php } ?>

                    <h1><?php the_title(); ?></h1>

                    <div class="content single_post_content">
                        <?php the_content(); ?>
                    </div>

                    <?php
                    if(is_array($faq_r)) {

                        ?>
                        <div class="faq">
                        <h3 class="span_title_big">

                            <?php
                            if($faq['faq_title']) {
                                echo $faq['faq_title'];
                            } else{
                                echo 'שאלות נפוצות';
                            }
                            ?>

                        </h3>

                        <?php

                        $faq_i = 1;
                        foreach($faq_r as $faq) {
                        ?>

                            <a href="#" class="faq-question" data-question="faq-<?= $faq_i; ?>"><?= $faq['question']; ?></a>
                            <div class="faq-answer" data-question="faq-<?= $faq_i; ?>">
                                <?= $faq['answer']; ?>
                            </div>

                    <?php
                        $faq_i++;
                        }

                        ?></div><?php

                    } ?>

                    <div class="smalltext">
                        <?php
                        $leave_feedback = get_field('leave_feedback', 'options');

                        if($leave_feedback) {
                            ?>
                            <div class="leave_feedback">
                                <p><strong>* יש לכם הצעות לעדכון/תיקון התוכן?</strong> אנא כתבו לנו <a href="#feedback-popup" data-lity>כאן</a></p>
                            </div>
                        <?php } ?>
                        <?php the_field('art-text', 'options'); ?>
                    </div>

                    <div class="content_info<?= $wrapper_class; ?>">

                        <div class="article-form" id="contactform">
                            <span class="title">פנייה לייעוץ ראשוני <span class="break-mobile">ללא התחייבות</span></span>

                            <div class="article-form-wrap">
                                <?php echo do_shortcode('[contact-form-7 title="טופס אחרי מאמר"]'); ?>
                            </div>

                        </div>

                        <?php if(!$infobox_hide) { ?>
                            <div class="infobox">
                                <!-- Big Image -->
                                <?php if( !empty($big_image) ): ?>

                                    <img src="<?php echo $big_image['url']; ?>" class="big-image" alt="<?php echo $big_image['alt']; ?>" />

                                <?php endif; ?>
                                <!-- Big Image -->
                                <div class="lawyer_name">
                                    <?= LAWYER_NAME; ?>
                                </div
                                <?php the_field('gray-box', 'options'); ?>
                            </div>
                        <?php } ?>

                    </div>

                </div>
                <?php


            }
        }
        ?>
    </div>

</div>

<?php print_subjects_icons(get_the_ID()); ?>
<?php print_success_articles(get_the_ID()); ?>
<?php print_related_articles(get_the_ID()); ?>

<?php
get_footer();
