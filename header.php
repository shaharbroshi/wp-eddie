<?php
global $post;

$codes = get_field('global_codes', 'options');
$thankyoupage = get_field('thankyou_link', 'options');

// Header is short or fullwidth?
$header_style = get_field('header_style', 'options');

$header_width = '';
if($header_style == "fullwidth"){
    $header_width = " fullwidth";
}

// Find out if logo is on the right or in the header?
$logo_version = get_field('logo_version', 'options');
$logo_draw = get_field('logo_draw', 'options');

$header_class = '';
if($logo_version == "up"){
    $header_class = "uplogo";
}

// Big Image
$big_image = get_field('big-image', 'options');

// Logo
$logo = get_field('page-logo', 'options');
$logo_link = get_field('logo_link', 'options');

if(is_array($logo_link)){ $link = $logo_link['url']; }else{ $link = home_url(); }

$colors = get_field('colors', 'options');

$header_colors = $colors['header_colors'];
$footer_colors = $colors['footer_colors'];
$articles_colors = $colors['articles_colors'];

// Faq
$faq = get_field('faq', $post->ID);
$faq_r = $faq['faq_r'];

$header_mobile_version = get_field('header_mobile_version', 'options');
$header2_colors = $colors['header2_colors'];

$quiz_colors = $colors['quiz_color'];

$mobile_menu = get_field('mobile_menu', 'options');
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html id="ie7" class="ie" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" class="ie" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 9]>
<html id="ie9" class="ie" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) | !(IE 9) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="icon" href="<?= $colors['site_favicon']['url']; ?>" type="image/x-icon">

    <?php
    if(is_array($faq_r)) {

        ?>
        <script type="application/ld+json">
            {
              "@context": "https://schema.org",
              "@type": "FAQPage",
              "mainEntity": [
                 <?php $i = 1; foreach($faq_r as $faq) {

                    $question_final = str_replace(array("'", "\"", "&quot;"), "", strip_tags($faq['question']) );
                    $answer_final = str_replace(array("'", "\"", "&quot;"), "", strip_tags($faq['answer']) );

                ?>
                  {
                    "@type": "Question",
                    "name": "<?= $question_final; ?>",
                    "acceptedAnswer": {
                      "@type": "Answer",
                      "text": "<?= $answer_final; ?>"
                    }
                  }<?php if ($i < count($faq_r)) { echo ','; } ?>
                <?php $i++; } ?>
              ]
            }

        </script>
        <?php

    }
    ?>

    <?php if($codes['head_code']) { ?>
    <!-- Global Header Codes -->
        <?= $codes['head_code']; ?>
    <!-- Global Header Codes -->
    <?php } ?>

    <?php if($codes['home_only'] && is_front_page()) { ?>
        <!-- Home Only Code -->
        <?= $codes['home_only']; ?>
        <!-- Home Only Code -->
    <?php } ?>

    <?php
    if(is_array($thankyoupage)) {
        $thankyou_id = url_to_postid($thankyoupage['url']);

        ?>
        <script>
            document.addEventListener( 'wpcf7mailsent', function( event ) {
                location = '<?= get_permalink($thankyou_id); ?>';
            }, false );
        </script>

        <?php

        if($thankyou_id == $post->ID){
            $thankyou_class = "thankyou";
            ?>
            <!-- Thankyou page Header code -->
            <?= $codes['thankyou_head']; ?>
            <!-- Thankyou page Header code -->
            <?php
        }

    }
    ?>

    <?php wp_head(); ?>

    <style>

        :root{
            --toc-color: <?= $colors['toc_color']; ?>;
            --text-color: <?= $colors['btn_send']; ?>;
            --main-quiz-color: <?= $quiz_colors['main_quiz_color']; ?>;
        }

        .header_mobile .header_dial{background:<?= $colors['mobile_phone_bg']; ?>}
        .header_mobile_btn span{background:<?= $colors['mobile_hamburger']; ?>}
        .footer_mobile_strip a.phone{background:<?= $colors['mobile_footer_callnow']; ?>;color:<?= $colors['mobile_footer_callnow_text']; ?>}
        .footer_mobile_strip a.contact{background:<?= $colors['mobile_footer_contact']; ?>;color:<?= $colors['mobile_footer_contact_text']; ?>}
        .footer_mobile_strip a.whatsapp{background:<?= $colors['mobile_footer_whatsapp']; ?>;color:<?= $colors['mobile_footer_whatsapp_text']; ?>}
        .pthumb-icon{background:<?= $colors['header_phone_bg']; ?>;}
        .quickdial .ref_phone{color:<?= $colors['header_phone_bg_text']; ?>;}
        form.wpcf7-form .wpcf7-submit{background:<?= $colors['btn_send']; ?>;}
        .fixed_strip{background:<?= $colors['footer_desktop']; ?>;}
        header, .header_mobile{background:<?= $header_colors['bgcolor']; ?>;}
        .menu > ul > li > a, .menu > ul > li .sub-menu a, .menu_mobile > ul > li a, .menu_mobile > ul > li.active.menu-item-has-children > a{color:<?= $header_colors['linkcolor']; ?>;}
        .menu > ul > li.menu-item-has-children:after, .menu_mobile > ul > li.menu-item-has-children:after{border-right:2px solid <?= $header_colors['linkcolor']; ?>;border-top:2px solid <?= $header_colors['linkcolor']; ?>;}
        .menu > ul > li > a:hover, .menu > ul > li .sub-menu a:hover{color:<?= $header_colors['linkcolor_hover']; ?>;}
        footer{background:<?= $footer_colors['footer_color']; ?>;}
        footer .footer_cols .fcol .fcol-title{color:<?= $footer_colors['footer_header']; ?>;}
        footer .footer_cols .fcol .fcol-menu li a{color:<?= $footer_colors['footer_link']; ?>;}
        footer .footer_cols .fcol .fcol-text{color:<?= $footer_colors['footer_link']; ?>;}
        .related-articles .span_title_big:before{background:<?= $articles_colors['icon_color']; ?>}

        <?php
            if($logo_version == "up"){
                ?>.content_wrap .block_right .big_image{padding:25px 0;background-color: #e9eaec;}<?php
            }
        ?>

        <?php
            if($header_mobile_version == "header2"){
                ?>
                .header_mobile_btn_wrap_color{background:<?= $header2_colors['hamburger_bgcolor'] ?>;}
                .header_mobile_btn span{background:<?= $header2_colors['hamburger_strips_colors'] ?>;}
                .header_bottom .cta a{background:<?= $header2_colors['contact_bgcolor']; ?>;}
                .header_bottom .cta a{color:<?= $header2_colors['contact_textcolor']; ?>;}
                <?php
            }
        ?>

    </style>
</head>

<body <?php body_class($thankyou_class); ?> data-header="<?= $header_mobile_version; ?>">

<?php
$ref = $_GET['ref'];
$cookie_phone = get_field('manage_links', 'options');

if(is_array($cookie_phone)){

    foreach($cookie_phone as $cphone){

        if($cphone['link_source'] == $ref){
            $data_phone = $cphone['link_phone'];
        }

    }

}

if($ref){
    ?>
    <div class="cookie-ref" data-ref="<?= $ref; ?>" class="cookie_input"></div>
    <div class="cookie-phone" data-phone="<?= $data_phone; ?>" class="cookie_input"></div>
    <?php
}
?>

<div class="site_wrap">

    <header class="<?= $header_class; ?><?= $header_width; ?>">
        <div class="container header">
            <?php
            if($logo_version == "up"){
                ?>
                <div class="logo">

                    <?php if (isset($logo_draw['url']) && $logo_draw['url']) { ?>
                        <img src="<?= $logo_draw['url']; ?>" alt="<?= $logo_draw['alt']; ?>">
                    <?php } ?>

                    <!-- Logo -->
                    <?php if( !empty($logo) ): ?>
                        <a href="<?= $link; ?>"><img src="<?php echo $logo['url']; ?>" class="logo-image" alt="<?php echo $logo['alt']; ?>" /></a>
                    <?php endif; ?>
                    <!-- Logo -->
                </div>
                <?php
            }
            ?>
            <div class="menu">
                <?php wp_nav_menu( array( 'menu' => 'top-menu', 'menu_class' => 'nav-menu', 'container' => '' ) ); ?>
            </div>
            <div class="quickdial">
                <a href="#" class="pthumb phone">
                    <span class="pthumb-icon"><img src="<?= asset('images/phone_icon.png'); ?>" /></span>
                    <?php echo do_shortcode('[phone]'); ?>
                </a>
            </div>
        </div>
    </header>

    <div class="header_mobile <?= $header_mobile_version; ?>">

        <?php if($header_mobile_version == "header2") { ?>

            <div class="header_upper">

                <div class="header_mobile_btn_wrap">
                    <div class="header_mobile_btn_wrap_color">
                        <div class="header_mobile_btn">
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </div>

                <div class="thumb">

                    <?php
                    if($logo_version == "up"){

                        if (isset($logo_draw['url']) && $logo_draw['url']) {
                            ?><img src="<?= $logo_draw['url']; ?>" alt="<?= $logo_draw['alt']; ?>"><?php
                        }

                    }else {

                        ?>
                        <a href="<?= $link; ?>"><img src="<?php echo $big_image['url']; ?>" class="big-image rounded" alt="<?php echo $big_image['alt']; ?>" /></a>
                        <?php

                    }

                    ?>

                </div>

                <div class="logo">
                    <!-- Logo -->
                    <?php if( !empty($logo) ): ?>
                        <a href="<?= $link; ?>"><img src="<?php echo $logo['url']; ?>" class="logo-image" alt="<?php echo $logo['alt']; ?>" /></a>
                    <?php endif; ?>
                    <!-- Logo -->
                </div>

            </div>

            <div class="header_bottom">

                <div class="text">
                    <span><?= $header2_colors['text_above_dial']; ?></span>

                    <a href="#" class="header2_dial phone"><?php echo do_shortcode('[phone]'); ?></a>
                </div>

                <div class="cta">
                    <a href="#contactform" class="contact">
                        <?= $header2_colors['contact_text']; ?>
                    </a>
                </div>

            </div>

        <?php }else{ ?>

            <div class="header_mobile_btn">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>

            <a href="#" class="header_dial phone"><img src="<?= asset('images/phone.svg'); ?>" /></a>

            <div class="thumb">

                <?php
                if($logo_version == "up"){

                    if (isset($logo_draw['url']) && $logo_draw['url']) {
                        ?><img src="<?= $logo_draw['url']; ?>" alt="<?= $logo_draw['alt']; ?>"><?php
                    }

                }else {

                    ?>
                    <a href="<?= $link; ?>"><img src="<?php echo $big_image['url']; ?>" class="big-image rounded" alt="<?php echo $big_image['alt']; ?>" /></a>
                    <?php

                }

                ?>

            </div>

            <div class="logo">
                <!-- Logo -->
                <?php if( !empty($logo) ): ?>
                    <a href="<?= $link; ?>"><img src="<?php echo $logo['url']; ?>" class="logo-image" alt="<?php echo $logo['alt']; ?>" /></a>
                <?php endif; ?>
                <!-- Logo -->
            </div>

        <?php } ?>

    </div>

    <div class="menu_mobile">
        <?php wp_nav_menu( array( 'menu' => 'menu-mobile-only', 'menu_class' => 'nav-menu', 'container' => '' ) ); ?>

        <?php if (isset($mobile_menu['image']['url']) && $mobile_menu['image']['url']) { ?>
            <div class="menu_mobile_image">
                <?php if($mobile_menu['image_url']) { ?><a href="<?= $mobile_menu['image_url']; ?>" <?php if($mobile_menu['image_url_blank']){ ?>target="_blank"<?php } ?>><?php } ?>
                    <img src="<?= $mobile_menu['image']['url']; ?>" alt="<?= $mobile_menu['image']['alt']; ?>">
                <?php if($mobile_menu['image_url']) { ?></a><?php } ?>
            </div>
        <?php } ?>
    </div>
