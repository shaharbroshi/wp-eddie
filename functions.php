<?php
# Install theme options
include('inc/theme-options.php');

# Init my templates
include('inc/init.php');

# Init quiz
include('inc/quiz.php');

$site_url = get_site_url();

if($site_url != 'http://eddiecopy.test'){

    # ACF
    include('inc/acf.php');

}

/* Exclude One Content Type From Yoast SEO Sitemap */
function sitemap_exclude_post_type( $value, $post_type ) {
    if ( $post_type == 'attachment' ) return true;
}
add_filter( 'wpseo_sitemap_exclude_post_type', 'sitemap_exclude_post_type', 10, 2 );

add_action( 'template_redirect', 'test_attachment_redirect', 10 );
function test_attachment_redirect() {
    if( is_attachment() ) {
        $url = wp_get_attachment_url( get_queried_object_id() );
        wp_redirect( $url, 301 );
    }
    return;
}

// Enable hidden formates buttons to WP visual editor
function myprefix_mce_buttons_2( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}

add_filter( 'mce_buttons_2', 'myprefix_mce_buttons_2', 1000, 2 );

add_filter('tiny_mce_before_init', 'wpse3882_tiny_mce_before_init');
function wpse3882_tiny_mce_before_init($settings)
{
    $settings['theme_advanced_blockformats'] = 'p,h1,h2,h3,h4';

    // From http://tinymce.moxiecode.com/examples/example_24.php
    $style_formats = array(
        array('title' => 'כותרת סרטון', 'block' => 'div', 'classes' => 'iframe_title', 'styles' => array('color' => '#000', 'background-color' => '#e9eaec', 'text-align' => 'center', 'padding' => '12px 2px', 'font-size' => '18px', 'font-weight' => 'bold')),
    );
    // Before 3.1 you needed a special trick to send this array to the configuration.
    // See this post history for previous versions.
    $settings['style_formats'] = json_encode( $style_formats );

    return $settings;
}

// add hidden fields to all cf7 forms.
add_action('wpcf7_form_hidden_fields', 'addUTMS_wpcf7cf_form_hidden_fields',10,1);
function addUTMS_wpcf7cf_form_hidden_fields($hidden_fields) {

    $current_form = wpcf7_get_current_contact_form();
    $current_form_id = $current_form->id();

    global $wp;
    $site_url = home_url( $wp->request );

    return array(
        'site_details' => $site_url,
    );
}
