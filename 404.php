<?php
get_header();

?>

    <div class="content_wrap container">

        <?php print_block_right(); ?>

        <div class="block_left">

            <div class="error404">

                <div class="page_content">

                    <?php if ( function_exists('yoast_breadcrumb') ) { ?>
                        <div class="breadcrumbs">
                            <?php yoast_breadcrumb(); ?>
                        </div>
                    <?php } ?>

                    <div class="error404-content">

                        <img src="<?= asset('images/error404.png'); ?>" />

                        <h1>אופס, הדף שחיפשת לא נמצא</h1>

                        <a href="<?= home_url(); ?>" class="btn">לדף הבית</a>

                    </div>


                </div>


            </div>

        </div>

    </div>

<?php
get_footer();
