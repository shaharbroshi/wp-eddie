<?php

/**
 * Add Theme support
 */
add_theme_support( 'title-tag' );
add_theme_support( 'post-thumbnails' );

add_filter( 'get_site_icon_url', '__return_false' );

/**
 * Locate to Assets Directory
 */
function asset($file = NULL) {
    return get_template_directory_uri() . '/assets/' . $file;
}

/**
 * Remove Emoji from header
 */
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

/**
 * Enqueue Assets
 */
function binternet_scripts() {

    /**
     * Styles Only
     */

    // Main CSS
    wp_enqueue_style( 'main', asset() . 'css/main.css', array(), THEME_FILES_VERSION );

    /**
     * JS Files - In Header
     */

//    wp_deregister_script('jquery');

    // Main JS
    wp_enqueue_script( 'jquery-js', asset() . 'js/jquery.min.js', array(), THEME_FILES_VERSION );

    // Main JS
    wp_enqueue_script( 'main-js', asset() . 'js/main.js', array(), THEME_FILES_VERSION );

    // Cookie JS
    wp_enqueue_script( 'cookie-js', asset() . 'js/jquery.cookie.js', array('main-js'), THEME_FILES_VERSION );

    // Toc Min JS
    wp_enqueue_script( 'toc-min-js', asset() . 'js/tocbot.min.js', array('main-js'), THEME_FILES_VERSION );

    // Magnific Popup
    wp_enqueue_script('magnific-js', asset() . 'js/jquery.magnific-popup.min.js', array('main-js'), THEME_FILES_VERSION);

    // Toc JS
    wp_enqueue_script( 'toc-js', asset() . 'js/toc.js', array('main-js'), THEME_FILES_VERSION );

}
add_action( 'wp_enqueue_scripts', 'binternet_scripts' );

/**
 * Load CSS in footer
 */
function prefix_add_footer_styles() {
    // Main CSS
    wp_enqueue_style( 'embed-player', asset() . 'css/parts/embed-player.css', array(), THEME_FILES_VERSION );
};
add_action( 'get_footer', 'prefix_add_footer_styles' );

/**
 * Install Menu support on theme
 */
function register_my_menu() {
    register_nav_menu('main-menu',__( 'Main Menu' ));
}
add_action( 'init', 'register_my_menu' );

/**
 * Disable Gutenberg
 */
// disable for posts
add_filter('use_block_editor_for_post', '__return_false', 10);
// disable for post types
add_filter('use_block_editor_for_post_type', '__return_false', 10);
// disable for posts
add_filter('gutenberg_can_edit_post', '__return_false', 10);
// disable for post types
add_filter('gutenberg_can_edit_post_type', '__return_false', 10);

// Disable gutenberg style in Front
function wps_deregister_styles() {
    wp_dequeue_style( 'wp-block-library' );
}
add_action( 'wp_print_styles', 'wps_deregister_styles', 100 );

/**
 * Add ACF General Settings
 */
if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' 	=> 'אפשרויות כלליות',
        'menu_title'	=> 'אפשרויות כלליות',
        'menu_slug'     => 'theme-settings'
    ));

}

/**
 * Allow SVG Files Upload
 */
function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/**
 * Generate [phone] shortcode
 */
function foobar_func( $atts ){
    return "<span class='ref_phone'>" . get_field('site_phone_normal', 'options') . "</span>";
}
add_shortcode( 'phone', 'foobar_func' );
add_filter('acf/format_value/type=text', 'do_shortcode');

/**
 * Generate [toc] shortcode
 */
function generate_toc(){

    ob_start();

    ?>
    <div class="post__toc">

        <div class="post__toc-title">תוכן עניינים</div>

        <div class="post__toc-explore" data-toc-titles="h3"></div>

        <div class="post__toc-expand" style="display:none;">
            <a href="#" class="post__toc-expand-items">עוד</a>
        </div>

    </div>
    <?php

    return ob_get_clean();

}
add_shortcode( 'toc', 'generate_toc' );

/**
 * Print block right
 */

function print_block_right(){

    // Logo
    $logo = get_field('page-logo', 'options');
    $logo_link = get_field('logo_link', 'options');
    $image_link = get_field('image_link', 'options');

    $logo_version = get_field('logo_version', 'options');

    if(is_array($logo_link)){ $link = $logo_link['url']; }else{ $link = home_url(); }
    if(is_array($image_link)){ $image_url = $image_link['url']; }else{ $image_url = home_url(); }

    // Big Image
    $big_image = get_field('big-image', 'options');

    ?>
    <div class="block_right">

        <?php if($logo_version != "up") { ?>

        <div class="logo">

            <!-- Logo -->
            <?php if( !empty($logo) ): ?>

                <a href="<?= $link; ?>"><img src="<?php echo $logo['url']; ?>" class="logo-image" alt="<?php echo $logo['alt']; ?>" /></a>

            <?php endif; ?>
            <!-- Logo -->

        </div>

        <?php } ?>

        <div class="big_image">

            <!-- Big Image -->
            <?php if( !empty($big_image) ): ?>

                <a href="<?= $image_url; ?>"><img src="<?php echo $big_image['url']; ?>" class="big-image" alt="<?php echo $big_image['alt']; ?>" /></a>

            <?php endif; ?>
            <!-- Big Image -->

            <div class="lawyer_name">
                <?= LAWYER_NAME; ?>
            </div>

        </div>

        <div class="contact_box">
            <span class="title"><?php the_field('contact-title', 'options'); ?></span>

            <div class="contact_form">
                <?php echo do_shortcode('[contact-form-7 title="טופס יצירת קשר צדדי דסקטופ"]'); ?>
            </div>
        </div>

        <div class="contant_details">

            <?php
            $contact_details = get_field('contact-details', 'options');

            if($contact_details){

                ?>
                <span class="span_title">פרטי יצירת קשר:</span>

                <div class="address">
                    <?= $contact_details; ?>
                </div>
                <?php

            }
            ?>

            <span class="span_title_big"><?= get_field('title-moreinfo', 'options'); ?></span>
            <div class="side_menu">
                <?php wp_nav_menu( array( 'menu' => 'side-menu', 'menu_class' => 'side-menu' ) ); ?>
            </div>
        </div>

    </div>
    <?php

}

/**
 * Print Site map
 */
function print_site_map(){

    global $post;

    $args = array(
        'post_type' => 'page',
        'post_status' => 'publish',
        'numberposts' => -1,
        'posts_per_page' => 999,
        'orderby' => 'title',
        'order'   => 'ASC'
    );
    $the_query = new WP_Query( $args );

    if ( $the_query->have_posts() ) :

        ?><ul><?php

        while ( $the_query->have_posts() ) : $the_query->the_post();

            ?><li><a href="<?= get_permalink(get_the_ID()); ?>" title="<?= get_the_title(); ?>"><?= get_the_title(); ?></a></li><?php

        endwhile;
    else:
        echo 'sorry, no pages found';
    endif;

        ?></ul><?php

    wp_reset_postdata();

}

/**
 * Get post first image
 */

function catch_that_image($post_id) {

    $content = get_post_field('post_content', $post_id);
    $first_img = '';
    $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $content, $matches);
    $first_img = $matches [1] [0];

    if(empty($first_img)){ //Defines a default image
        $first_img = "/images/default.jpg";
    }
    return $first_img;
}

/**
 * Remove Nofollow for stage copy
 */
/**
 * Set blog pages noindex,nofollow
 */
add_filter("wpseo_robots", function($robots) {

    $site_url = get_site_url();

    if($site_url == "http://eddiecopy.bdev.co.il"){
        return 'nofollow';
    }

    return $robots;

});

/**
 * Related articles
 */
function print_related_articles($post_id){

    $related_articles_manually = get_field('related_articles_manually', $post_id);
    $current_page_related_articles = get_field('related_articles', $post_id);

    // Get Global Articles Query
    $global_articles = get_field('related_articles', 'options');
    $articles_query = $global_articles['articles_r'];
    $articles_title = $global_articles['title'];
    $articles_show = $global_articles['show_wide'];
    $background_color = $global_articles['background_color'];
    $title_color = $global_articles['title_color'];

    if($related_articles_manually){
        $articles_query = $current_page_related_articles['articles_r'];
        $articles_title = $current_page_related_articles['title'];
        $background_color = $current_page_related_articles['background_color'];
        $title_color = $current_page_related_articles['title_color'];
        $articles_show = true;
    }


    if($articles_show) {
        ?>
        <div class="related-articles container" style="background-color:<?php echo $background_color; ?>">

<span class="span_title_big" style="color:<?php echo $title_color; ?>;<?php if(isset($background_color) && $background_color != "" && strtolower($background_color) != "#ffffff") {?>border-top:0;<?php } ?>"><?= $articles_title; ?></span>
            <div class="articles">

                <?php if (isset($articles_query) && $articles_query) { ?>
                    <?php foreach($articles_query as $item) : ?>

                        <div class="item">
                            <div class="thumb">

                                <?php if (isset($item['link']['url']) && $item['link']['url']) { ?>
                                    <a href="<?= $item['link']['url']; ?>" <?php if (isset($item['link']['target']) && $item['link']['target']) { ?>target="<?= $item['link']['target']; ?>"<?php } ?>>
                                <?php } ?>

                                    <?php if (isset($item['image']['url']) && $item['image']['url']) { ?>
                                        <img src="<?= $item['image']['url']; ?>" alt="<?= $item['image']['alt']; ?>">
                                    <?php } ?>

                                        <div class="title"><?= $item['link']['title']; ?></div>

                                <?php if (isset($item['link']['url']) && $item['link']['url']) { ?>
                                    </a>
                                <?php } ?>

                            </div>
                        </div>

                    <?php endforeach; ?>
                <?php } ?>

            </div>

        </div>
        <?php
    }

}

/**
 * Success Articles
 */
function print_success_articles($post_id){

    $success_articles_manually = get_field('success_articles_manually', $post_id);
    $current_page_success_articles = get_field('success_articles', $post_id);

    // Get Global Articles Query
    $global_articles = get_field('success_articles', 'options');

    $articles_query = $global_articles['articles_r'];
    $articles_title = $global_articles['title'];
    $articles_show = $global_articles['show_wide'];
    $background_color = $global_articles['background_color'];
    $title_color = $global_articles['title_color'];


    if($success_articles_manually){
        $articles_query = $current_page_success_articles['articles_r'];
        $articles_title = $current_page_success_articles['title'];
        $background_color = $current_page_success_articles['background_color'];
        $title_color = $current_page_success_articles['title_color'];
        $articles_show = true;
    }

    if($articles_show) {
        ?>
        <div class="related-articles container" style="background-color:<?php echo $background_color; ?>">

<span class="span_title_big" style="color:<?php echo $title_color; ?>;<?php if(isset($background_color) && $background_color != "" && strtolower($background_color) != "#ffffff") {?>border-top:0;<?php } ?>"><?= $articles_title; ?></span>
            <div class="articles">

                <?php if (isset($articles_query) && $articles_query) { ?>
                    <?php foreach($articles_query as $item) : ?>

                        <div class="item">
                            <div class="thumb">

                                <?php if (isset($item['link']['url']) && $item['link']['url']) { ?>
                                <a href="<?= $item['link']['url']; ?>" <?php if (isset($item['link']['target']) && $item['link']['target']) { ?>target="<?= $item['link']['target']; ?>"<?php } ?>>
                                    <?php } ?>

                                    <?php if (isset($item['image']['url']) && $item['image']['url']) { ?>
                                        <img src="<?= $item['image']['url']; ?>" alt="<?= $item['image']['alt']; ?>">
                                    <?php } ?>

                                    <div class="title"><?= $item['link']['title']; ?></div>

                                    <?php if (isset($item['link']['url']) && $item['link']['url']) { ?>
                                </a>
                            <?php } ?>

                            </div>
                        </div>

                    <?php endforeach; ?>
                <?php } ?>

            </div>

        </div>
        <?php
    }

}

/**
 * Subjects Icons
 */
function print_subjects_icons($post_id){

    $more_articles_manually = get_field('more_subjects_manually', $post_id);
    $current_page_subjects = get_field('subjects_r', $post_id);

    // Get Global Articles Query
    $global_articles = get_field('subjects_articles', 'options');
    $articles_query = $global_articles['articles_r'];
    $articles_title = $global_articles['title'];
    $articles_show = $global_articles['show_wide'];
    $background_color = $global_articles['background_color'];
    $title_color = $global_articles['title_color'];
    $icon_background = $global_articles['icon_background'];
    $icon_stroke = $global_articles['icon_stroke'];


    if($more_articles_manually){
        $articles_query = $current_page_subjects['articles_r'];
        $articles_title = $current_page_subjects['title'];
        $background_color = $current_page_subjects['background_color'];
        $title_color = $current_page_subjects['title_color'];
        $icon_background = $current_page_subjects['icon_background'];
        $icon_stroke = $current_page_subjects['icon_stroke'];
        $articles_show = true;
    }

    if($articles_show) {
        ?>
        <div class="more-subjects container" style="background-color:<?php echo $background_color; ?>">

<span class="span_title_big" style="color:<?php echo $title_color; ?>;<?php if(isset($background_color) && $background_color != "" && strtolower($background_color) != "#ffffff") {?>border-top:0;<?php } ?>"><?= $articles_title; ?></span>
            <div class="articles">

                <?php if (isset($articles_query) && $articles_query) { ?>
                    <?php foreach($articles_query as $item) : ?>

                    <div class="item">

                        <?php if (isset($item['link']['url']) && $item['link']['url']) { ?>
                            <a style="color:<?php echo $title_color; ?>" href="<?= $item['link']['url']; ?>" <?php if (isset($item['link']['target']) && $item['link']['target']) { ?>target="<?= $item['link']['target']; ?>"<?php } ?>>

                                <?php if (isset($item['image']['url']) && $item['image']['url']) { ?>
                                    <span class="icon" style="border:2px solid <?php echo $icon_stroke; ?>">
                                        <img src="<?= $item['image']['url']; ?>" alt="<?= $item['image']['alt']; ?>">
                                    </span>
                                <?php } ?>

                                <?= $item['link']['title']; ?>

                            </a>
                        <?php } ?>

                    </div>

                    <?php endforeach; ?>
                <?php } ?>

            </div>

        </div>
        <?php
    }

}

/**
 * Add an HTML class to MediaElement.js container elements to aid styling.
 *
 * Extends the core _wpmejsSettings object to add a new feature via the
 * MediaElement.js plugin API.
 */
add_action( 'wp_print_footer_scripts', 'mytheme_mejs_add_container_class' );

function mytheme_mejs_add_container_class() {
    if ( ! wp_script_is( 'mediaelement', 'done' ) ) {
        return;
    }
    ?>
    <script>
        (function() {
            var settings = window._wpmejsSettings || {};
            settings.features = settings.features || mejs.MepDefaults.features;
            settings.features.push( 'exampleclass' );
            MediaElementPlayer.prototype.buildexampleclass = function( player ) {
                player.container.addClass( 'mytheme-mejs-container' );
            };
        })();
    </script>
    <?php
}
