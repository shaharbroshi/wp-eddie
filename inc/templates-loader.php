<?php
/**
 * Templates loader
 */

$templates_to_load = array(
    'sitemap'
);

foreach($templates_to_load as $template) :
    require THEME_TEMPLATES . '/'.$template.'/init.php';
endforeach;