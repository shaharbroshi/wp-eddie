<?php

define( 'THEME_PATH' , get_template_directory() );
define( 'THEME_URI' , get_stylesheet_directory_uri() );
define( 'THEME_INC' , THEME_PATH . '/inc' );
define( 'THEME_CORE' , THEME_INC . '/core' );
define( 'THEME_TEMPLATES' , THEME_INC . '/templates' );
define( 'THEME_PARTS' , THEME_INC . '/parts' );
define( 'THEME_COMPONENTS' , THEME_INC . '/components' );
define( 'THEME_FILES_VERSION', '1232443111');

$lawyer_name = get_field('lawyer_name', 'options');

define( 'LAWYER_NAME', $lawyer_name );

require THEME_INC . '/templates-loader.php';

function get_part($template_file, $custom_field = '') {
    $custom_field_name = $custom_field;
    require THEME_PARTS . '/' . $template_file;
    unset($custom_field_name);
}