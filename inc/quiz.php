<?php
function quiz_shortcode() {
    // Check if quiz should be shown
    if (!get_field('show_quiz')) {
        return '';
    }

    // Get title and subtitle from ACF
    $quiz_title = get_field('title');
    $quiz_subtitle = get_field('subtitle');
    $thankyou_title = get_field('thankyou_title');
    $thankyou_button = get_field('thankyou_button');

    // Get the global counter value, default to 101 if not set
    $quiz_counter = get_option('quiz_global_users_count', 101);
    if (empty($quiz_counter)) {
        $quiz_counter = 101;
        update_option('quiz_global_users_count', $quiz_counter);
    }

    // Enqueue Font Awesome
    wp_enqueue_style('font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css');

    $questions = array();
    if (have_rows('quiz_r')):
        while (have_rows('quiz_r')): the_row();
            $answers = array();
            $correctAnswer = '';

            if (have_rows('answers_r')):
                while (have_rows('answers_r')): the_row();
                    $answer = get_sub_field('answer');
                    $answers[] = $answer;

                    if (get_sub_field('answer_correct')) {
                        $correctAnswer = $answer;
                    }
                endwhile;
            endif;

            $questions[] = array(
                'question' => get_sub_field('question'),
                'answers' => $answers,
                'correctAnswer' => $correctAnswer
            );
        endwhile;
    endif;

    $questions_json = json_encode($questions);

    ob_start();
    ?>
    <div class="quiz-container">
        <i class="fa-regular fa-lightbulb quiz-icon"></i>
        <div class="quiz-content">
            <div class="quiz-header">
                <div class="quiz-users-count"><?php echo esc_html($quiz_counter); ?> גולשים השתתפו באתגר</div>
                <div class="quiz-title"><?php echo esc_html($quiz_title); ?></div>
                <div class="quiz-subtitle"><?php echo esc_html($quiz_subtitle); ?></div>
            </div>
            <div class="quiz-question">
                <div class="question-header">
                    <div class="question-title"></div>
                </div>
                <div class="answers-container"></div>
            </div>
            <div class="thank-you-message" style="display: none;">
                <div class="thankyou-title"><?php echo esc_html($thankyou_title); ?></div>
                <?php if (isset($thankyou_button['url']) && $thankyou_button['url']) { ?>
                    <a href="<?= $thankyou_button['url']; ?>" <?php if (isset($thankyou_button['target']) && $thankyou_button['target']) { ?>target="<?= $thankyou_button['target']; ?>"<?php } ?>><?= $thankyou_button['title']; ?></a>
                <?php } ?>
            </div>
        </div>
    </div>

    <script>
        jQuery(document).ready(function($) {
            const quizQuestions = <?php echo $questions_json; ?>;
            let currentQuestionIndex = 0;
            let isAnswered = false;

            function displayQuestion(questionIndex) {
                const question = quizQuestions[questionIndex];
                $('.question-title').text(question.question);

                const $answersContainer = $('.answers-container');
                $answersContainer.empty();

                question.answers.forEach(answer => {
                    $answersContainer.append(`
                    <div class="answer-option" data-answer="${answer}">
                        <span class="answer-text">${answer}</span>
                        <span class="answer-icon">
                            <i class="fa-solid fa-check check-icon"></i>
                            <i class="fa-solid fa-xmark x-icon"></i>
                        </span>
                    </div>
                `);
                });
            }

            function handleAnswer(selectedAnswer) {
                if (isAnswered) return;

                isAnswered = true;
                const currentQuestion = quizQuestions[currentQuestionIndex];
                const $selectedOption = $(`.answer-option[data-answer="${selectedAnswer}"]`);
                const $correctOption = $(`.answer-option[data-answer="${currentQuestion.correctAnswer}"]`);

                if (selectedAnswer === currentQuestion.correctAnswer) {
                    $selectedOption.addClass('answer-correct');
                    $selectedOption.find('.x-icon').hide();
                } else {
                    $selectedOption.addClass('answer-wrong');
                    $selectedOption.find('.check-icon').hide();
                    $correctOption.addClass('answer-correct');
                    $correctOption.find('.x-icon').hide();
                }

                setTimeout(function() {
                    if (currentQuestionIndex < quizQuestions.length - 1) {
                        currentQuestionIndex++;
                        isAnswered = false;
                        displayQuestion(currentQuestionIndex);
                    } else {
                        // Create nonce in PHP and pass it to JavaScript
                        const nonce = '<?php echo wp_create_nonce("quiz_counter_nonce"); ?>';

                        // Send Ajax request to update counter
                        $.ajax({
                            url: '<?php echo admin_url("admin-ajax.php"); ?>',
                            type: 'POST',
                            data: {
                                action: 'update_quiz_counter',
                                nonce: nonce
                            }
                        });

                        $('.quiz-header').hide();
                        $('.quiz-question').hide();
                        $('.thank-you-message').show();
                    }
                }, 2500);

            }

            $('.quiz-container').on('click', '.answer-option', function() {
                const selectedAnswer = $(this).data('answer');
                handleAnswer(selectedAnswer);
            });

            if (quizQuestions.length > 0) {
                displayQuestion(currentQuestionIndex);
            }
        });
    </script>
    <?php
    return ob_get_clean();
}
add_shortcode('quiz', 'quiz_shortcode');

function update_quiz_counter() {
    // Verify nonce for security
    if (!isset($_POST['nonce']) || !wp_verify_nonce($_POST['nonce'], 'quiz_counter_nonce')) {
        wp_send_json_error('Invalid nonce');
        die();
    }

    $current_count = get_option('quiz_global_users_count', 101);
    $new_count = intval($current_count) + 1;
    update_option('quiz_global_users_count', $new_count);

    wp_send_json_success();
    die();
}
add_action('wp_ajax_update_quiz_counter', 'update_quiz_counter');
add_action('wp_ajax_nopriv_update_quiz_counter', 'update_quiz_counter');
?>