<?php
/**
 * Enqueue
 */

add_action( 'wp_enqueue_scripts', function() {
    $page_template = basename(get_page_template());

    if ($page_template == "template-sitemap.php") :

        // Template
        wp_enqueue_style( 'template-sitemap', get_stylesheet_directory_uri() .'/assets/css/template-sitemap.css');

    endif;
}, 101 );

?>