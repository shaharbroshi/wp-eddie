<?php
get_header();

global $post;

// Sidebar location - right or left?
$mainwrap_location = get_field('mainwrap_location', 'options');

$mainwrap_class = '';
if($mainwrap_location == "left"){
    $mainwrap_class = " reverse";
}
?>

    <div class="content_wrap container<?= $mainwrap_class; ?>">

        <?php print_block_right(); ?>

        <div class="block_left">
            <?php
            if ( have_posts() ) {
                while ( have_posts() ) {
                    the_post();

                    ?>
                    <div class="page_content">

                        <h1><?php the_title(); ?></h1>

                        <div class="content">

                            <div class="sitemap">
                                <?php print_site_map(); ?>
                            </div>

                        </div>

                        <div class="side_menu_mobile">
                            <span class="span_title_big"><?= get_field('title-moreinfo', 'options'); ?></span>
                            <?php wp_nav_menu( array( 'menu' => 'side-menu', 'menu_class' => 'side-menu' ) ); ?>
                        </div>

                    </div>
                    <?php


                } // end while
            } // end if
            ?>
        </div>

    </div>

<?php
get_footer();


