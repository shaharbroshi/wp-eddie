// Gulp Libraries
let gulp = require( 'gulp' );
let notify = require( 'gulp-notify' );
let prefixer = require( 'gulp-autoprefixer' );
let scss = require( 'gulp-sass' );
let plumber = require( 'gulp-plumber' );
let browsersync = require( 'browser-sync' ).create();
var combine_mq = require( 'gulp-group-css-media-queries' );


// Gulp Variables
let PROJECT_URL = 'http://eddiecopy.test';
let PROJECT_PORT = 8888;
let LISTEN_FILES = [ '**/*.php' ];

let SRC_SCSS = '__src/scss/**/*.scss';
let DEST_CSS = 'assets/css';


// Gulp Tasks

/**
 * Compile SCSS files into one css file.
 */
gulp.task( 'build-scss', function () {

	return gulp
		.src( SRC_SCSS )
		.pipe( plumber() )
		.pipe( scss() )
		.on('error', function (err) {
			console.log(err.toString());
			this.emit('end');
		})
    .pipe( combine_mq() )
		.pipe( prefixer({
                browsers: [
                  "> 0.3%",
                  "last 7 versions",
                 "Android >= 4",
                 "Firefox >= 20",
                 "iOS >= 8"
                ],
               flexbox: true,
              }) )
		.pipe( gulp.dest( DEST_CSS ) )
		.pipe( browsersync.stream() )
		.pipe( notify( '### Finished Building SCSS ###' ) );

} );


/**
 * Listen to file changes and reload the page.
 */
gulp.task( 'browser-sync', function () {

	browsersync.init( {
		proxy : PROJECT_URL,
		port : PROJECT_PORT,
		injectChanges : true
	} );

	gulp.watch( LISTEN_FILES ).on( 'change', browsersync.reload );
	gulp.watch(SRC_SCSS, gulp.series('build-scss'));
} );


//gulp.task( 'default', ['browser-sync']);
gulp.task('default', gulp.series('browser-sync'));
